package main

import (
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	"github.com/go-redis/redis"
)

func main() {
	rc := NewClient()
	defer rc.Close()
	for index := 2000; index > 0; index-- {
		indexCopy := index
		go readConcurrent(rc, "index"+strconv.Itoa(indexCopy))
	}
	var input string
	fmt.Scanln(&input)
	//batchInsert(rc)
}

// NewClient creates a new redis client
func NewClient() *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:         os.Getenv("REDISENDPOINT"),
		Password:     os.Getenv("REDISPASSWORD"),
		DB:           0, // use default DB,
		PoolSize:     3,
		WriteTimeout: time.Duration(10 * time.Second),        // setting connection pool for write timeout to 10 seconds
		ReadTimeout:  time.Duration(50 * 1000 * 1000 * 1000), // setting connection pool for read timeout to 50 seconds
	})
	return client
}

func readConcurrent(rc *redis.Client, key string) {
	strCmd := rc.Get(key)
	in, err := strCmd.Int()
	if err != nil {
		log.Println("error in reading: ", err)
	}
	fmt.Println("value for key: ", key, in)
}

func batchInsert(rc *redis.Client) {
	for index := 0; index < 1000; index++ {
		statusCmd := rc.Set("index"+strconv.Itoa(index), index+1000, 0)
		fmt.Println(statusCmd.String())
	}
}
